package main

import (
	"fmt"
	"sort"
)

func main() {
	str := "jfbkbdjjjGGGSSSSSjjdЯаоооооВВЯЯ"
	ans := make(map[rune]int)
	for _, r := range str {
		ans[r]++
	}

	var keys []int
	for k := range ans {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	for _, k := range keys {
		fmt.Printf("%s%d", string(k), ans[rune(k)])
	}
}
