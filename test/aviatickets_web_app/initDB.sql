CREATE DATABASE aviaticket;
USE aviaticket;

CREATE USER 'aticket_user'@'localhost' IDENTIFIED BY 'edXkmG';
GRANT ALL ON aviaticket.* TO 'aticket_user'@'localhost';

create table flights
(
    flights_id  int auto_increment
        primary key,
    departure   varchar(255) not null,
    destination varchar(255) not null,
    date_from   datetime     not null,
    date_to     datetime     not null,
    status      varchar(255) not null
);

create table passengers
(
    passengers_id int auto_increment
        primary key,
    first_name    varchar(255) not null,
    last_name     varchar(255) not null
);
create table tickets
(
    tickets_id    int auto_increment
        primary key,
    flights_id    int not null,
    passengers_id int not null,
    baggage       int null,
    price         int not null
);
