package main
import (
    "fmt"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "net/http"
    "html/template"
    "log"
    "github.com/gorilla/mux"
)
type Flight struct{
    Id int
    Departure string
    Destination string
    Date_from string
    Date_to string
    Status string
}

type Ticket struct{
    Id int
    Flights_id int
    Passengers_id int
    Baggage int
    Price int
}

type Passenger struct{
    Id int
    First_name string
    Last_name string
}

var database *sql.DB
 
func DeleteHandler(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    _, err := database.Exec("delete from aviaticket.flights where flights_id = ?", id)
    if err != nil{
        log.Println(err)
    }
     
    http.Redirect(w, r, "/", 301)
}

func DeleteHandlerPassengers(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    _, err := database.Exec("delete from aviaticket.passengers where passengers_id = ?", id)
    if err != nil{
        log.Println(err)
    }
     
    http.Redirect(w, r, "/passengers", 301)
}

func DeleteHandlerTickets(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    _, err := database.Exec("delete from aviaticket.tickets where tickets_id = ?", id)
    if err != nil{
        log.Println(err)
    }
     
    http.Redirect(w, r, "/tickets", 301)
}
 
func EditPage(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    row := database.QueryRow("select * from aviaticket.flights where flights_id = ?", id)
    flight := Flight{}
    err := row.Scan(&flight.Id, &flight.Departure, &flight.Destination, &flight.Date_from, &flight.Date_to, &flight.Status)
    if err != nil{
        log.Println(err)
        http.Error(w, http.StatusText(404), http.StatusNotFound)
    }else{
        tmpl, _ := template.ParseFiles("templates/edit.html")
        tmpl.Execute(w, flight)
    }
}

func EditPagePassengers(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    row := database.QueryRow("select * from aviaticket.passengers where passengers_id = ?", id)
    passenger := Passenger{}
    err := row.Scan(&passenger.Id, &passenger.First_name, &passenger.Last_name)
    if err != nil{
        log.Println(err)
        http.Error(w, http.StatusText(404), http.StatusNotFound)
    }else{
        tmpl, _ := template.ParseFiles("templates/editpassenger.html")
        tmpl.Execute(w, passenger)
    }
}

func EditPageTickets(w http.ResponseWriter, r *http.Request) {
    vars := mux.Vars(r)
    id := vars["id"]
 
    row := database.QueryRow("select * from aviaticket.tickets where tickets_id = ?", id)
    ticket := Ticket{}
    err := row.Scan(&ticket.Id, &ticket.Flights_id, &ticket.Passengers_id, &ticket.Baggage, &ticket.Price)
    if err != nil{
        log.Println(err)
        http.Error(w, http.StatusText(404), http.StatusNotFound)
    }else{
        tmpl, _ := template.ParseFiles("templates/editticket.html")
        tmpl.Execute(w, ticket)
    }
}
 
func EditHandler(w http.ResponseWriter, r *http.Request) {
    err := r.ParseForm()
    if err != nil {
        log.Println(err)
    }
    id := r.FormValue("id")
    departure := r.FormValue("departure")
    date_from := r.FormValue("date_from")
    destination := r.FormValue("destination")
    date_to := r.FormValue("date_to")
    status := r.FormValue("status")
 
    _, err = database.Exec("update aviaticket.flights set departure=?, date_from=?, destination=?, date_to=?, status=? where flights_id = ?", 
        departure, date_from, destination, date_to, status, id)
 
    if err != nil {
        log.Println(err)
    }
    http.Redirect(w, r, "/", 301)
}

func EditHandlerPassengers(w http.ResponseWriter, r *http.Request) {
    err := r.ParseForm()
    if err != nil {
        log.Println(err)
    }
    id := r.FormValue("id")
    first_name := r.FormValue("first_name")
    last_name := r.FormValue("last_name")
 
    _, err = database.Exec("update aviaticket.passengers set first_name=?, last_name=? where passengers_id = ?", 
        first_name, last_name, id)
 
    if err != nil {
        log.Println(err)
    }
    http.Redirect(w, r, "/passengers", 301)
}

func EditHandlerTickets(w http.ResponseWriter, r *http.Request) {
    err := r.ParseForm()
    if err != nil {
        log.Println(err)
    }
    id := r.FormValue("id")
    flights_id := r.FormValue("flights_id")
    passengers_id := r.FormValue("passengers_id")
    baggage := r.FormValue("baggage")
    price := r.FormValue("price")
 
    _, err = database.Exec("update aviaticket.tickets set flights_id=?, passengers_id=?, baggage=?, price=? where tickets_id = ?", 
        flights_id, passengers_id, baggage, price, id)
 
    if err != nil {
        log.Println(err)
    }
    http.Redirect(w, r, "/tickets", 301)
}
 
func CreateHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        err := r.ParseForm()
        if err != nil {
            log.Println(err)
        }
        departure := r.FormValue("departure")
        date_from := r.FormValue("date_from")
        destination := r.FormValue("destination")
        date_to := r.FormValue("date_to")
        status := r.FormValue("status")
 
        _, err = database.Exec("insert into aviaticket.flights (departure, date_from, destination, date_to, status) values (?, ?, ?, ?, ?)", 
          departure, date_from, destination, date_to, status)
 
        if err != nil {
            log.Println(err)
        }
        http.Redirect(w, r, "/", 301)
    }else{
        http.ServeFile(w,r, "templates/create.html")
    }
}

func CreateHandlerPassengers(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        err := r.ParseForm()
        if err != nil {
            log.Println(err)
        }
        first_name := r.FormValue("first_name")
        last_name := r.FormValue("last_name")
 
        _, err = database.Exec("insert into aviaticket.passengers (first_name, last_name) values (?, ?)", first_name, last_name)
 
        if err != nil {
            log.Println(err)
        }
        http.Redirect(w, r, "/passengers", 301)
    }else{
        http.ServeFile(w,r, "templates/createpassenger.html")
    }
}

func CreateHandlerTickets(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        err := r.ParseForm()
        if err != nil {
            log.Println(err)
        }
        flights_id := r.FormValue("flights_id")
        passengers_id := r.FormValue("passengers_id")
        baggage := r.FormValue("baggage")
        price := r.FormValue("price")
 
        _, err = database.Exec("insert into aviaticket.tickets (flights_id, passengers_id, baggage, price) values (?, ?, ?, ?)", flights_id, passengers_id, baggage, price)
 
        if err != nil {
            log.Println(err)
        }
        http.Redirect(w, r, "/tickets", 301)
    }else{
        http.ServeFile(w,r, "templates/createticket.html")
    }
}
 
func IndexHandler(w http.ResponseWriter, r *http.Request) {
 
    rows, err := database.Query("select * from aviaticket.flights")
    if err != nil {
        log.Println(err)
    }
    defer rows.Close()
    flights := []Flight{}
     
    for rows.Next(){
        p := Flight{}
        err := rows.Scan(&p.Id, &p.Departure, &p.Destination, &p.Date_from, &p.Date_to,  &p.Status)
        if err != nil{
            fmt.Println(err)
            continue
        }
        flights = append(flights, p)
    }
 
    tmpl, _ := template.ParseFiles("templates/index.html")
    tmpl.Execute(w, flights)
}

func IndexHandlerPassengers(w http.ResponseWriter, r *http.Request) {
 
    rows, err := database.Query("select * from aviaticket.passengers")
    if err != nil {
        log.Println(err)
    }
    defer rows.Close()
    passengers := []Passenger{}
     
    for rows.Next(){
        p := Passenger{}
        err := rows.Scan(&p.Id, &p.First_name, &p.Last_name)
        if err != nil{
            fmt.Println(err)
            continue
        }
        passengers = append(passengers, p)
    }
 
    tmpl, _ := template.ParseFiles("templates/indexpassengers.html")
    tmpl.Execute(w, passengers)
}

func IndexHandlerTickets(w http.ResponseWriter, r *http.Request) {
 
    rows, err := database.Query("select * from aviaticket.tickets")
    if err != nil {
        log.Println(err)
    }
    defer rows.Close()
    tickets := []Ticket{}
     
    for rows.Next(){
        p := Ticket{}
        err := rows.Scan(&p.Id, &p.Flights_id, &p.Passengers_id, &p.Baggage, &p.Price)
        if err != nil{
            fmt.Println(err)
            continue
        }
        tickets = append(tickets, p)
    }
 
    tmpl, _ := template.ParseFiles("templates/indextickets.html")
    tmpl.Execute(w, tickets)
}
 
func main() {
      
    db, err := sql.Open("mysql", "aticket_user:edXkmG@/aviaticket")
     
    if err != nil {
        log.Println(err)
    }
    database = db
    defer db.Close()
     
    router := mux.NewRouter()
    router.HandleFunc("/", IndexHandler)
    router.HandleFunc("/create", CreateHandler)
    router.HandleFunc("/edit/{id:[0-9]+}", EditPage).Methods("GET")
    router.HandleFunc("/edit/{id:[0-9]+}", EditHandler).Methods("POST")
    router.HandleFunc("/delete/{id:[0-9]+}", DeleteHandler)

    router.HandleFunc("/passengers", IndexHandlerPassengers)
    router.HandleFunc("/createpassenger", CreateHandlerPassengers)
    router.HandleFunc("/editpassenger/{id:[0-9]+}", EditPagePassengers).Methods("GET")
    router.HandleFunc("/editpassenger/{id:[0-9]+}", EditHandlerPassengers).Methods("POST")
    router.HandleFunc("/deletepassenger/{id:[0-9]+}", DeleteHandlerPassengers)

    router.HandleFunc("/tickets", IndexHandlerTickets)
    router.HandleFunc("/createticket", CreateHandlerTickets)
    router.HandleFunc("/editticket/{id:[0-9]+}", EditPageTickets).Methods("GET")
    router.HandleFunc("/editticket/{id:[0-9]+}", EditHandlerTickets).Methods("POST")
    router.HandleFunc("/deleteticket/{id:[0-9]+}", DeleteHandlerTickets)
     
    http.Handle("/",router)
 
    fmt.Println("Server is listening...")
    http.ListenAndServe(":8080", nil)
}
